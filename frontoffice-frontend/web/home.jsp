<%-- 
    Document   : home
    Created on : 15 juin 2021, 16:40:00
    Author     : Ibrahim
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Online Store - Accueil du front-office</title>
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/application.js"></script>
    </head>
    <body>
        <h1>OnlineStore, votre boutique multimédia en ligne</h1>
        <a href="catalogue">Accès au catalogue des oeuvres</a>
    </body>
</html>
