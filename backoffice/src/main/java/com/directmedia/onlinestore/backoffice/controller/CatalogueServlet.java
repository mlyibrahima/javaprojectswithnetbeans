/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.directmedia.onlinestore.backoffice.controller;

import com.directmedia.onlinestore.core.entity.Artist;
import com.directmedia.onlinestore.core.entity.Catalogue;
import com.directmedia.onlinestore.core.entity.Work;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import javax.servlet.DispatcherType;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ibrahim
 */
@WebServlet(name = "CatalogueServlet", urlPatterns = {"/catalogue"})
public class CatalogueServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                
        if (Catalogue.listOfWorks.isEmpty()) {
            Artist tomCruise= new Artist("Tom Cruise");
            Artist michaelJackson= new Artist("Michael Jackson");
            Artist louisDeFunes= new Artist("Louis De Funès");

            Work minorityReport = new Work("Minority Report");
            Work bad =  new Work("Bad");
            Work leGendarmeDeSaintTropez = new Work("Le gendarme de Saint-Tropez");

            minorityReport.setMainArtist(tomCruise);
            bad.setMainArtist(michaelJackson);
            leGendarmeDeSaintTropez.setMainArtist(louisDeFunes);

            minorityReport.setRelease(2002);
            bad.setRelease(1987);
            leGendarmeDeSaintTropez.setRelease(1964);
           
            minorityReport.setSummary("Le film place le spectateur dans un futur proche cyberpunk, une dystopie dont le cadre se situe en 2054 à Washington D.C (États-Unis), où trois êtres humains mutants, les précogs, peuvent prédire les crimes à venir grâce à leur don de précognition. Grâce à ces visions du futur, la ville a réussi à éradiquer la criminalité et les agents de l'organisation gouvernementale Précrime peuvent arrêter les criminels juste avant qu’ils ne commettent leurs méfaits. Mais un jour, le chef de l'unité John Anderton reçoit des précogs une vision le concernant : dans moins de 36 heures, il aura assassiné un homme qu’il ne connaît pas encore "
                    + "et pour une raison qu’il ignore. Choqué, il prend la fuite, poursuivi par ses coéquipiers qui ont pour mission de l’arrêter conformément au système.");
            bad.setSummary("La vie des jeunes fugueurs, des drogués, et des sans-abris de Séoul est chroniquée dans ce docudrame du réalisateur Jang Sun-woo."
                    + " Avec ses images d'auditions et d'interviews impromptues de véritables personnes vivant dans la rue, mélangées à des scènes faites avec des acteurs non-professionnels qui jouent les adolescents troublés, Bad Movie est un portrait improvisé mais très évocateur des vies rebelles de ces âmes abandonnées. ");
            leGendarmeDeSaintTropez.setSummary("Imaginé par Richard Balducci après une rencontre insensée avec un gendarme assez débonnaire en poste à Saint-Tropez, le film raconte les aventures de Ludovic Cruchot, un gendarme très zélé, muté dans la cité balnéaire de Saint-Tropez, sur la côte d'Azur, avec le grade de maréchal des logis-chef. Il y découvre une brigade où il fait bon vivre et participe aux récurrentes chasses aux nudistes et aux nombreuses activités détente de sa brigade, dirigée par l'adjudant Gerber, quelque peu dépassé.\n" +
"\n" +
"Ludovic Cruchot est interprété par Louis de Funès, autour duquel tout le film a été construit. L'adjudant Gerber est joué par Michel Galabru et les autres gendarmes par Jean Lefebvre, Christian Marin et le duo Grosso et Modo. Nicole, la fille de Cruchot, est incarnée par Geneviève Grad. Conçu comme une « petite comédie sans prétention », avec un budget peu élevé, le film est tourné de juin à juillet 1964, en extérieurs à Belvédère et à Saint Tropez ainsi qu'aux studios de la Victorine (Nice). La bande originale est composée par Raymond Lefebvre et comprend la chanson Douliou-douliou Saint Tropez, qui remportera un franc succès.\n" +
"\n" +
"Sorti en salles le 9 septembre 1964, Le Gendarme de Saint-Tropez rencontre à la surprise générale un succès considérable, arrivant en tête du box-office français de l'année 1964 avec plus de 7,8 millions d'entrées. L'accueil critique est partagé mais Louis de Funès remporte une Victoire du cinéma pour son interprétation. Installé pour la première fois en haut du box-office, l'acteur voit sa carrière et sa célébrité définitivement lancées.\n" +
"\n" +
"Ce triomphe inattendu entraînera la réalisation d'une suite, Le Gendarme à New York, dès l'année suivante, puis d'autres, formant finalement une série composée de six films, dont le dernier est sorti en 1982. ");

            minorityReport.setGenre("Science Fiction");
            bad.setGenre("Pop");
            leGendarmeDeSaintTropez.setGenre("Comédie");

            //Catalogue.listOfWorks = new HashSet<Work>();
            Catalogue.listOfWorks.add(minorityReport);
            Catalogue.listOfWorks.add(bad);
            Catalogue.listOfWorks.add(leGendarmeDeSaintTropez);
        }
        /*request.setAttribute("listedesoeuvres", Catalogue.listOfWorks);
        RequestDispatcher disp = request.getRequestDispatcher("/WEB-INF/catalogue.jsp");
        disp.forward(request, response);*/
        
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.writeValue(out, Catalogue.listOfWorks);            
    }
}
