/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.directmedia.onlinestore.backoffice.controller;

import com.directmedia.onlinestore.core.entity.Artist;
import com.directmedia.onlinestore.core.entity.Catalogue;
import com.directmedia.onlinestore.core.entity.Work;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Optional;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ibrahim
 */
@WebServlet(name = "AddWorkServlet", urlPatterns = {"/add-work"})
public class AddWorkServlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html; charset=UTF-8");
        
        boolean success=true;
        
        String title = request.getParameter("title");
        String release = request.getParameter("release");  
        String genre = request.getParameter("genre");
        String summary = request.getParameter("summary");
        String mainArtist = request.getParameter("mainArtist");
       
        
        Work work = new Work(title);
        try {
            work.setRelease(Integer.parseInt(release));
        }
        catch (NumberFormatException nfe){
           success=false;
        }
        work.setGenre(genre);
        work.setSummary(summary);
        
        Artist artist = new Artist(mainArtist);
        work.setMainArtist(artist);
             
        /*for (Work wk : Catalogue.listOfWorks) {
         if (wk.getTitle().equals(work.getTitle()) && 
                wk.getRelease()==work.getRelease() && wk.getMainArtist().getName().equals(work.getMainArtist().getName())) {
            success=false;
         }
        }*/
        
        Optional<Work> optionalWork=Catalogue.listOfWorks.stream().
                filter(wk -> wk.getTitle().equals(work.getTitle()) && 
                        wk.getRelease()==work.getRelease() && 
                        wk.getMainArtist().getName().equals(work.getMainArtist().getName())).findAny();
        if (optionalWork.isPresent()) {
            success=false;
        }
        
        RequestDispatcher disp = null;
        if (success){
            Catalogue.listOfWorks.add(work);
            request.setAttribute("identifiantOeuvre", work.getId());
            disp = request.getRequestDispatcher("/work-added-success");    
        }
        else {
            disp = request.getRequestDispatcher("/work-added-failure");
        }
        disp.forward(request, response);      
    }
}
