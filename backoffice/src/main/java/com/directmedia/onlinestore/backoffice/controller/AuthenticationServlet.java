/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.directmedia.onlinestore.backoffice.controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Ibrahim
 */
@WebServlet(name = "AuthenticationServlet", urlPatterns = {"/login"})
public class AuthenticationServlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html; charset=UTF-8");
        HttpSession session = request.getSession();
        String numeroSession = session.getId();
        
        String login = request.getParameter("login");
        session.setAttribute("login", login);
        String password = request.getParameter("password");
        
        PrintWriter out=response.getWriter();
        out.print("<HTML><BODY>");
        
        if(login.equals("michel")  && password.equals("123456") || login.equals("caroline") && password.equals("abcdef")){
            out.print("<a href=\"home.jsp\">Aller à la page d'accueil</a>");
        }
        else {
            out.print("login / mdp erroné <BR/>");
            out.print("<a href=\"login.html\">Essayez à nouveau</a>");
        }
        
        out.print("</BODY></HTML>");
    }
}
