/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.travauxpratiques.firstapp.controller;

import com.travauxpratiques.firstappcore.Personne;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ibrahim
 */
@WebServlet(name = "TestThreadSafeServlet", urlPatterns = "/testThread")
public class TestThreadSafeServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html; charset=UTF-8");
        
        String montant=request.getParameter("montant");
        
        try {
            Thread.sleep(10000);
        } catch (InterruptedException ex) {
            Logger.getLogger(TestThreadSafeServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        PrintWriter out= response.getWriter();
        out.print("<HTML><BODY>Vous avez retiré "+montant+" euros</BODY></HTML>"); 
    }
}
