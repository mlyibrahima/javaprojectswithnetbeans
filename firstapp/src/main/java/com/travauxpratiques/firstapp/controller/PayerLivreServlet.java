/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.travauxpratiques.firstapp.controller;

import com.travauxpratiques.firstappcore.Personne;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Ibrahim
 */
@WebServlet(name = "PayerLivreServlet", urlPatterns = "/payer-livre")
public class PayerLivreServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session=request.getSession();
        String numeroSession=session.getId();
        response.setContentType("text/html; charset=UTF-8");
        
        String numeroCarte=request.getParameter("numeroCarte");
        String identifiantLivre = (String)session.getAttribute("identifiantLivre");
        
        //session.setAttribute("identifiantLivre", null);
        //    session.removeAttribute("identifiantLivre");
        
        //session.invalidate();
        
        PrintWriter out= response.getWriter();
        out.print("<HTML><BODY>Paiement effectué avec le numéro de carte "+numeroCarte+"<BR/>");
        out.print("Le livre payé est le livre numéro "+identifiantLivre+"<BR/>");
        out.print("Votre numéro de session est "+numeroSession+"<BR/>");
        out.print("</BODY></HTML>"); 
    }
}
