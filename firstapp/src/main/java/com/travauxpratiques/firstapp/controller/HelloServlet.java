/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.travauxpratiques.firstapp.controller;

import com.travauxpratiques.firstappcore.Personne;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Ibrahim
 */
@WebServlet(name = "HelloServlet", urlPatterns = {"/hello"}, loadOnStartup = 1)
public class HelloServlet extends HttpServlet {
    
    Date dateInitialisation;

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config); 
        dateInitialisation = new Date();
    }

   
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html; charset=UTF-8");
        PrintWriter out= response.getWriter();
        Personne michelDupont = new Personne("Dupont", "Michel");
        out.print("<HTML><BODY>Bonjour " +michelDupont.getFullName()+"<BR/>");
        out.print("Ce résultat vous est servi par une Servlet instanciée le "+dateInitialisation);
        out.print("</BODY></HTML>"); 
    }
}
