/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.travauxpratiques.firstapp.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.travauxpratiques.firstappcore.Produit;
import com.travauxpratiques.firstappcore.Somme;
import com.travauxpratiques.firstappcore.SommeEtProduit;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author HP
 */
@WebServlet(name = "Somme", urlPatterns = {"/somme"})
public class SommeServlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String nombre1 = request.getParameter("nombre1");
        String nombre2 = request.getParameter("nombre2");
        try {
            int sommeNumerique = Integer.parseInt(nombre1) + Integer.parseInt(nombre2);
            int produitNumerique = Integer.parseInt(nombre1) * Integer.parseInt(nombre2);
            
            String format=request.getParameter("format");
            
            if (format!=null && format.equals("pdf")) {
                request.setAttribute("somme", sommeNumerique);
                RequestDispatcher disp=request.getRequestDispatcher("/pdf");
                disp.forward(request, response);
            }
            else {
                response.setContentType("application/json");
                PrintWriter out=response.getWriter();
                ObjectMapper objectMapper = new ObjectMapper();
                
                SommeEtProduit sommeEtProduit=new SommeEtProduit();
                Somme somme = new Somme();
                somme.setNumerique(sommeNumerique);
                somme.setTexte("douze");
                
                Produit produit = new Produit();
                produit.setNumerique(produitNumerique);
                produit.setTexte("trente-deux");
                
                sommeEtProduit.setSomme(somme);
                sommeEtProduit.setProduit(produit);
                
                /*List<Somme> list = new ArrayList();
                list.add(somme);
                Somme somme2 = new Somme();
                somme2.setNumerique(55);
                somme2.setTexte("peu importe");
                list.add(somme2);*/
                
                objectMapper.writeValue(out, sommeEtProduit);
                //objectMapper.writeValue(out, list);
                        
                /*String message = "{\"somme\": {\"numerique\":"+somme+", \"texte\": \"douze\"},"
                        + "\"produit\": {\"numerique\":"+produit+", \"texte\": \"trente-six\"}}";
                out.print(message);*/
            }    
        }
        catch (NumberFormatException nfe) {
            RequestDispatcher disp=request.getRequestDispatcher("/unexpected-error");
            disp.forward(request, response);
        }
        
    }
}
