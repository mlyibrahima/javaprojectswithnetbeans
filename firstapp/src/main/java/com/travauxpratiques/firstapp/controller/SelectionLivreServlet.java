/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.travauxpratiques.firstapp.controller;

import com.travauxpratiques.firstappcore.Livre;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author HP
 */
@WebServlet(name = "SelectionLivreServlet", urlPatterns = {"/selection-livre"})
public class SelectionLivreServlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session=request.getSession();
        String numeroSession=session.getId();
        String identifiantLivre = request.getParameter("id");
        
        List<Livre> liste=(List<Livre>)session.getAttribute("listelivres");
        
        if (liste==null) {
            liste = new ArrayList<>();
            session.setAttribute("listelivres", liste);
        }
        
        Livre livre= new Livre();
        livre.setNumeroLivre(Integer.parseInt(identifiantLivre));
        liste.add(livre);
        response.setContentType("text/html; charset=UTF-8");
                        
        PrintWriter out=response.getWriter();
        out.print("<HTML><BODY>Merci d'avoir choisi le livre "+identifiantLivre+"<BR/>");
        //out.print("Votre numéro de session est "+numeroSession+"<BR/>");
        out.print("<A href=\"choix-du-livre.html\">Cliquez ici pour ajouter un autre livre</A><BR/>");
        out.print("<A href=\"payer-livre.html\">Cliquez ici pour payer</A></BODY></HTML>");                            
    }
}
