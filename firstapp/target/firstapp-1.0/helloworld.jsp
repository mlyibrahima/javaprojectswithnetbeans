<%-- 
    Document   : helloworld
    Created on : 26 mai 2021, 16:23:26
    Author     : HP
--%>

<%@page import="com.travauxpratiques.firstappcore.Personne"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Hello World!</h1>
        <%Personne michelDupont= new Personne("Michel", "Dupont");%>
        Bonjour <%=michelDupont.getFullName()%><br/>
    </body>
</html>
