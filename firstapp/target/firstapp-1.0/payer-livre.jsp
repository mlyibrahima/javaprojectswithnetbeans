<%-- 
    Document   : payer-livre
    Created on : 19 juil. 2021, 16:19:01
    Author     : HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Livre payé</title>
    </head>
    <body>
        Paiement effectué avec le numéro de carte <%=request.getParameter("numeroCarte")%><br/>
        Liste des livres payés : <BR/>
        <c:forEach items="${listelivres}" var="livre">
            Le livre payé est le livre numéro ${livre.numeroLivre}<BR/>
        </c:forEach>
    </body>
</html>
