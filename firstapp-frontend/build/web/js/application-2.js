$(document).ready(function(){
    $('input').last().on('click',function(){
        if ($("input[name=format]").prop( "checked")===true){
            $("form").submit();
        }
        else{
            $.ajax({
              url: "http://localhost:8080/firstapp/rest/calculation/somme-et-produit",
              data: $("form").serialize(),
              success: function(resultObject) {
                  var message="<p>La somme des 2 nombres fournis est <strong>"+resultObject.somme.numerique+"-"+resultObject.somme.texte+"</strong></p>";
                  message=message.concat("<p>Le produit des 2 nombres fournis est <strong>"+resultObject.produit.numerique+"-"+resultObject.produit.texte+"</strong></p>");
                  $("p").remove();
                  $("form").after(message);
              }
            });
        }
    });
});