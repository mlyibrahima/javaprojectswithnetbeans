<%-- 
    Document   : home
    Created on : 19 juil. 2021, 13:09:38
    Author     : HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Online Store - Accueil du backoffice</title>
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/application.js"></script>
    </head>
    <body>
        <h1>OnlineStore - Gestion de la boutique</h1>
            <a href="catalogue">Afficher le catalogue des oeuvres</a><br/>
            <a href="add-work-form.html">Ajouter une oeuvre au catalogue</a>
    </body>
</html>
