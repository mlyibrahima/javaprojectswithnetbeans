<%-- 
    Document   : work-details
    Created on : 20 juil. 2021, 22:35:23
    Author     : HP
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Detail de l'oeuvre</title>
    </head>
    <body>
        <h1>Descriptif de l'oeuvre</h1>
        Titre : ${requestScope.work.getTitle()} <br/>
        Année de parution: ${work.getRelease()}<br/>
        Genre : ${work.getGenre()}<br/>
        Artiste : ${work.getMainArtist().getName()}<br/>
        Résumé : ${work.getSummary()} <br/>
        <form action = "addToCart" method="POST">
            <INPUT type="hidden" name="identifiant" value="${work.id}"/>
            <INPUT type="submit" value="Ajouter au caddie"/>
        </form>
    </body>
</html>
