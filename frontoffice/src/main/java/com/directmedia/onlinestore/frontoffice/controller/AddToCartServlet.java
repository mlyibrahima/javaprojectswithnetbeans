/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.directmedia.onlinestore.frontoffice.controller;

import com.directmedia.onlinestore.core.entity.Catalogue;
import com.directmedia.onlinestore.core.entity.ShoppingCart;
import com.directmedia.onlinestore.core.entity.Work;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Ibrahim
 */
@WebServlet(name = "AddToCartServlet", urlPatterns = {"/addToCart"})
public class AddToCartServlet extends HttpServlet {

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html charset=UTF-8");
       
        String idAsString = request.getParameter("identifiant");
        long idAsLong = Long.parseLong(idAsString);
        
        ShoppingCart cart = (ShoppingCart)request.getSession().getAttribute("cart");
        
        if (cart==null) {
            cart=new ShoppingCart();
            request.getSession().setAttribute("cart", cart);
        }
        
        /*for (Work work : Catalogue.listOfWorks) {
            if(work.getId()==idAsLong) {
                cart.getItems().add(work);
                break;
            }
        }*/
        
        Optional<Work> optionalWork=Catalogue.listOfWorks.stream().filter(work -> work.getId()==idAsLong).findAny();
        if (optionalWork.isPresent()) {
            cart.getItems().add(optionalWork.get());
        }
        
        PrintWriter out=response.getWriter();
        out.print("<HTML><BODY>");
        out.print("Oeuvre ajoutée au caddie ("+cart.getItems().size()+")<BR/>");
        out.print("<A href=\"catalogue\">Retour au catalogue</A>");
        out.print("</BODY></HTML>");
    }
}
